import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SlotMachineService } from '../slot-machine.service';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-slot-machine',
  templateUrl: './slot-machine.component.html',
  styleUrls: ['./slot-machine.component.scss']
})
export class SlotMachineComponent implements OnInit {

  @Output() cashOutEvent = new EventEmitter<boolean>();

  constructor(private service: SlotMachineService,
    private toastService: ToastService) { }

  firstSlot: string;
  secondSlot: string;
  thirdSlot: string;

  ngOnInit(): void {
  }

  roll(): void {
    this.service.roll()
    .subscribe((res : RollResponse) => {
      const rolls = res.rollState.currentRoll;
      const playerState = res.playerState;
      this.firstSlot = rolls[0].label;
      this.secondSlot = rolls[1].label;
      this.thirdSlot = rolls[2].label;

      if (res.rollState.isWinningRoll) {
        console.log("WIN")
        this.toastService.show(`Nice! You won ${rolls[0].reward}`)
      } else {
        this.toastService.show(`Rolled! ${playerState.credits} remaining!`)
      }

      
    },(error) => {
      if(error.error.error.name === 'NotEnoughCreditsError') {
        this.toastService.show(`Sorry, no credits left!`)
      }
    })
  }

  cashOut(): void {
    this.service.cashOut().subscribe(res => {
      this.cashOutEvent.emit(true);
    })
  }

}


interface RollResponse {
  rollState : {
    currentRoll: {id: number, symbol: string, reward: number, label: string}[],
    isWinningRoll: boolean
  }, 
  playerState : {
    credits: number
  }
}