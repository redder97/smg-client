import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SlotMachineService {

  url: string = `${environment.roll}`

  constructor(private http: HttpClient) { }

  start(): Observable<any> {
    return this.http.get(`${this.url}/start`, {})
  }

  roll(): Observable<any> {
    return this.http.post(`${this.url}`, {});
  }

  cashOut(): Observable<any> {
    return this.http.post(`${this.url}/cash-out`, {});
  }
}
