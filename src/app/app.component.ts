import { Component } from '@angular/core';
import { SlotMachineService } from './slot-machine.service';
import { ToastService } from './toast.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  dataSource = null;
  gameStarted: boolean = false;

  constructor(private service: SlotMachineService,
    public toastService: ToastService) { }


  start(): void {
    this.service.start().subscribe((res) => {
      this.gameStarted = true;
      this.toastService.show(`Game Started with ${res.credits} credits`)
    });
  }

  handleCashOut(isCashedOut): void {
    if (isCashedOut) {
      this.gameStarted = false;
    }
  }


}
